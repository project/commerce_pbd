<?php

/**
 * @file
 * Views integration for Commerce PBD.
 */

/**
 * Implements hook_views_data().
 */
function commerce_pbd_views_data() {
  $data = array();

  $data['commerce_pbd_stock_level_report']['table']['group'] = t('Commerce PBD Stock Report');
  $data['commerce_pbd_stock_level_report']['table']['base'] = array(
    'field' => 'report_id',
    'title' => t('Commerce PBD Stock Report'),
    'help' => t('Stock Report from PBD retrieved via Web Service.'),
  );

  // Expose the report id.
  $data['commerce_pbd_stock_level_report']['report_id'] = array(
    'title' => t('Report ID'),
    'help' => t('The Stock Report ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_pbd_stock_level',
      'field' => 'report_id',
      'label' => t('Commerce PBD Item Stock Level'),
    ),
  );

  // Expose the created timestamp.
  $data['commerce_pbd_stock_level_report']['created'] = array(
    'title' => t('Report Date'),
    'help' => t('The date the Stock Report was created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // Expose the report status.
  $data['commerce_pbd_stock_level_report']['status'] = array(
    'title' => t('Stock Report Status'),
    'help' => t('The status of report.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Individual Items Stock Level.
  $data['commerce_pbd_stock_level']['table']['group'] = t('Commerce PBD Stock Availability');
  $data['commerce_pbd_stock_level']['table']['base'] = array(
    'field' => 'record_id',
    'title' => t('Commerce PBD Item Availability'),
    'help' => t('Stores PBD Item Availability associated with a product.'),
  );

  // For other base tables, explain how we join
  // 'left_field' is the primary key in the referenced table.
  // 'field' is the foreign key in this table.
  $data['commerce_pbd_stock_level']['table']['join'] = array(
    'commerce_pbd_stock_level_report' => array(
      'left_field' => 'report_id',
      'field' => 'report_id',
    ),
  );

  // Expose Fields.
  // Expose the product SKU.
  $data['commerce_pbd_stock_level']['sku'] = array(
    'title' => t('Product SKU'),
    'help' => t('The unique identifier of the product.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'field' => 'sku',
      'base' => 'commerce_product',
      'base field' => 'sku',
      'label' => t('Commerce Product'),
    ),
  );

  // Expose the stock value.
  $data['commerce_pbd_stock_level']['stock'] = array(
    'title' => t('In Stock'),
    'help' => t('The PBD Item Availability value.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose the Report ID.
  $data['commerce_pbd_stock_level']['report_id'] = array(
    'title' => t('PBD Stock Level Report ID'),
    'help' => t('The PBD Item Availability Report ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_pbd_stock_level_report',
      'field' => 'report_id',
      'label' => t('Commerce PBD Stock Level Report'),
    ),
  );

  // Expose the unit cost amount.
  $data['commerce_pbd_stock_level']['unit_cost_amount'] = array(
    'title' => t('Unit Cost Amount'),
    'help' => t('The unit cost amount.'),
    'field' => array(
      'handler' => 'commerce_pbd_views_handler_field_unit_cost_amount',
      'float' => TRUE,
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose the total cost amount.
  $data['commerce_pbd_stock_level']['total_cost_amount'] = array(
    'title' => t('Stock Item Total Cost'),
    'help' => t('The total cost amount.'),
    'field' => array(
      'handler' => 'commerce_pbd_views_handler_field_total_cost_amount',
      'float' => TRUE,
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose the currency.
  $data['commerce_pbd_stock_level']['unit_cost_currency_code'] = array(
    'title' => t('Stock Item Currency'),
    'help' => t('The Stock Item cost value currency.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose the created timestamp.
  $data['commerce_pbd_stock_level']['timestamp'] = array(
    'title' => t('Report Date'),
    'help' => t('The date the stock record was created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  return $data;
}
