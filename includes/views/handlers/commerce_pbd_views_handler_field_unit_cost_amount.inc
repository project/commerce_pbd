<?php

/**
 * Field handler to display Stock Item Total Cost.
 *
 * @ingroup views_field_handlers
 */
class commerce_pbd_views_handler_field_unit_cost_amount extends views_handler_field_numeric {

  /**
   * Implements handler init() function.
   */
  public function init(&$view, &$options) {
    parent::init($view, $options);
    $this->additional_fields['record_id'] = 'record_id';
    $this->additional_fields['unit_cost_currency_code'] = 'unit_cost_currency_code';
    $this->additional_fields['sku'] = 'sku';
    $this->additional_fields['unit_cost_amount'] = 'unit_cost_amount';
  }

  /**
   * Implements option_definition() function.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['set_precision']['default'] = TRUE;
    $options['precision']['default'] = 2;
    $options['empty_zero']['default'] = TRUE;
    $options['hide_empty']['default'] = TRUE;
    $options['prefix']['default'] = '$';
    return $options;
  }

  /**
   * Adds this term to the query.
   */
  public function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  /**
   * Implements handler get_value() function.
   */
  public function get_value($values, $field = NULL) {
    $alias = isset($field) ? $this->aliases[$field] : $this->field_alias;
    if (isset($values->{$alias})) {
      return $values->{$alias};
    }
    else {
      $currency_code = $this->get_value($values, 'unit_cost_currency_code');
      $commerce_unit_cost = $this->get_value($values, 'unit_cost_amount');
      if (isset($commerce_unit_cost)) {
        // We shouldn't use prefix or thousands separator here,
        // otherwise aggregation wouldn't work:
        $formatted_amount = number_format(commerce_currency_amount_to_decimal($commerce_unit_cost, $currency_code), $this->options['precision'], $this->options['decimal'], '');
        return $formatted_amount;
      }
    }
    return NULL;
  }

  /**
   * Performs an advanced text render for the item.
   */
  public function render_text($alter = array()) {
    $value = $this->last_render;
    if (strpos($value, $this->options['prefix']) === 0) {
      // views_aggregator sets fully rendered value with prefix
      // to ->last_render, so we'll remove the prefix to compare with 0.00:
      $value = substr($value, strlen($this->options['prefix']));
    }
    // Same with thousands separator: views_aggregator puts it in.
    if (strpos($value, $this->options['separator']) !== FALSE) {
      $value = str_replace(',', '', $value);
    }

    $zero = number_format(0, $this->options['precision'], $this->options['decimal'], $this->options['separator']);
    if ($value == $zero && $this->options['hide_empty'] && $this->options['empty_zero']) {
      return '';
    }
    // Adding prefix and thousands separator, if not NULL:
    if ($value == NULL) {
      return NULL;
    }
    elseif ($value >= 0) {
      return $this->options['prefix'] . number_format($value, $this->options['precision'], $this->options['decimal'], $this->options['separator']);
    }
    else {
      return '-' . $this->options['prefix'] . number_format(-$value, $this->options['precision'], $this->options['decimal'], $this->options['separator']);
    }
  }

  /**
   * Implements render() function.
   */
  public function render($values) {
    $decimal_value = $this->get_value($values);
    return isset($decimal_value) ? $decimal_value : NULL;
  }

  /**
   * Render a field using advanced settings.
   *
   * This renders a field normally, then MAY decide if render-as-link and
   * text-replacement rendering is necessary.
   */
  public function advanced_render($values) {

    $value = $this->render($values);
    if (is_array($value)) {
      $value = drupal_render($value);
    }

    $this->original_value = $value;
    $this->last_render = $value;

    $value = $this->render_text();

    if (is_array($value)) {
      $value = drupal_render($value);
    }

    return $value;
  }

}
