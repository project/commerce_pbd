<?php

/**
 * @file
 * Variables definition.
 */

/**
 * Implements hook_variable_info().
 */
function commerce_pbd_variable_info($options) {

  $variable['commerce_pbd_order_shipped_notify_customer_[mail_part]'] = array(
    'title' => t('Order Shipped Email (Customer)'),
    'type' => 'mail_html',
    'description' => t('Template for the email sent to a customer when the order has been shipped.'),
    'children' => array(
      'commerce_pbd_order_shipped_notify_customer_subject' => array(
        'default' => 'Your Order [commerce-order:order-number] at [site:name] has been shipped',
      ),
      'commerce_pbd_order_shipped_notify_customer_body' => array(
        'default' => array(
          'value' => '<p>Your order #[commerce-order:order-number] has been shipped</p>
                      <p>The tracking number is [commerce-order:commerce-pbd-tracking-number]</p>
                      <p>Your order details:</p><p>[commerce-order:commerce-email-order-items]</p>
                      <p>Please contact us if you have any questions about your order.</p>',
          'format' => 'full_html',
        ),
      ),
    ),
    'group' => 'commerce_email',
  );

  $variable['commerce_pbd_order_shipped_notify_admin_[mail_part]'] = array(
    'title' => t('Order Shipped Email (Admin)'),
    'type' => 'mail_html',
    'description' => t('Template for the email sent to administrator when the order has been shipped.'),
    'children' => array(
      'commerce_pbd_order_shipped_notify_admin_subject' => array(
        'default' => 'Order [commerce-order:order-number] has been shipped',
      ),
      'commerce_pbd_order_shipped_notify_admin_body' => array(
        'default' => array(
          'value' => '<p>The order #[commerce-order:order-number] has been shipped.</p>
                      <p>Its tracking number is [commerce-order:commerce-pbd-tracking-number]</p>
                      <p>You can view complete order at:
                      <a href="[site:url]admin/commerce/orders/[commerce-order:order-id]">
                      [site:url]admin/commerce/orders/[commerce-order:order-id]</a></p>',
          'format' => 'full_html',
        ),
      ),
    ),
    'group' => 'commerce_email',
  );

  $variable['commerce_pbd_products_backordered_notify_customer_[mail_part]'] = array(
    'title' => t('Products Backordered Email (Customer)'),
    'type' => 'mail_html',
    'description' => t('Template for the email sent to a customer when some products in the order have been backordered.'),
    'children' => array(
      'commerce_pbd_products_backordered_notify_customer_subject' => array(
        'default' => 'Some products in your Order [commerce-order:order-number] from [site:name] have been backordered',
      ),
      'commerce_pbd_products_backordered_notify_customer_body' => array(
        'default' => array(
          'value' => '<p>The following products in your Order #[commerce-order:order-number] have been backordered:</p>
                      <p>[commerce-order:commerce-pbd-backordered-items]</p>
                      <p>Please contact us if you have any questions about your order.</p>',
          'format' => 'full_html',
        ),
      ),
    ),
    'group' => 'commerce_email',
  );

  $variable['commerce_pbd_products_backordered_notify_admin_[mail_part]'] = array(
    'title' => t('Products Backordered Email (Admin)'),
    'type' => 'mail_html',
    'description' => t('Template for the email sent to administrator when some products in the order have been backordered.'),
    'children' => array(
      'commerce_pbd_products_backordered_notify_admin_subject' => array(
        'default' => 'Some products in the Order [commerce-order:order-number] have been backordered',
      ),
      'commerce_pbd_products_backordered_notify_admin_body' => array(
        'default' => array(
          'value' => '<p>The following products in the Order #[commerce-order:order-number] have been backordered:</p>
                      <p>[commerce-order:commerce-pbd-backordered-items]</p>
                      <p>You can view the complete order at:
                      <a href="[site:url]admin/commerce/orders/[commerce-order:order-id]">
                      [site:url]admin/commerce/orders/[commerce-order:order-id]</a></p>',
          'format' => 'full_html',
        ),
      ),
    ),
    'group' => 'commerce_email',
  );

  return $variable;
}
