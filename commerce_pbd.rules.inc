<?php

/**
 * @file
 * Defines Rules Events, Conditions and Actions.
 */

/**
 * Implements hook_rules_event_info().
 */
function commerce_pbd_rules_event_info() {
  $events = array();

  $events['commerce_order_shipping_authorized'] = array(
    'label' => t('Order Shipping Authorized'),
    'group' => t('Commerce Fulfillment'),
    'variables' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  $events['commerce_order_shipped'] = array(
    'label' => t('Order Shipped'),
    'group' => t('Commerce Fulfillment'),
    'variables' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  $events['commerce_products_shipped'] = array(
    'label' => t('Products Shipped'),
    'group' => t('Commerce Fulfillment'),
    'variables' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  $events['commerce_pbd_products_backordered'] = array(
    'label' => t('Products backordered'),
    'group' => t('Commerce Fulfillment'),
    'variables' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  return $events;
}

/**
 * Implements hook_rules_condition_info().
 */
function commerce_pbd_rules_condition_info() {
  return array(
    'commerce_pbd_order_in_pbd_system' => array(
      'base' => 'commerce_pbd_order_in_pbd_system',
      'label' => t('Order is in PBD system'),
      'parameter' => array(
        'commerce_order' => array('label' => t('Order'), 'type' => 'commerce_order'),
      ),
      'group' => t('Commerce Fulfillment'),
    ),
    'commerce_pbd_order_in_submit_queue' => array(
      'base' => 'commerce_pbd_order_in_submit_queue',
      'label' => t('Order is in the queue "submit to PBD system"'),
      'parameter' => array(
        'commerce_order' => array('label' => t('Order'), 'type' => 'commerce_order'),
      ),
      'group' => t('Commerce Fulfillment'),
    ),
    'commerce_pbd_order_rejected_by_pbd' => array(
      'base' => 'commerce_pbd_order_rejected_by_pbd',
      'label' => t('Order rejected by PBD system'),
      'parameter' => array(
        'commerce_order' => array('label' => t('Order'), 'type' => 'commerce_order'),
      ),
      'group' => t('Commerce Fulfillment'),
    ),
    'commerce_pbd_transaction_order_shippable' => array(
      'base' => 'commerce_pbd_transaction_order_shippable',
      'label' => t('Transaction belongs to Shippable Order'),
      'parameter' => array(
        'commerce_payment_transaction' => array('label' => t('Transaction'), 'type' => 'commerce_payment_transaction'),
      ),
      'group' => t('Commerce Fulfillment'),
    ),
    'commerce_pbd_transaction_order_in_pbd_system' => array(
      'base' => 'commerce_pbd_order_in_pbd_system',
      'label' => t('Transaction belongs to Order in PBD system'),
      'parameter' => array(
        'commerce_payment_transaction' => array('label' => t('Transaction'), 'type' => 'commerce_payment_transaction'),
      ),
      'group' => t('Commerce Fulfillment'),
    ),
    'commerce_pbd_transaction_order_rejected_by_pbd' => array(
      'base' => 'commerce_pbd_order_rejected_by_pbd',
      'label' => t('Transaction belongs to Order rejected by PBD system'),
      'parameter' => array(
        'commerce_payment_transaction' => array('label' => t('Transaction'), 'type' => 'commerce_payment_transaction'),
      ),
      'group' => t('Commerce Fulfillment'),
    ),
    'commerce_pbd_transaction_order_in_submit_queue' => array(
      'base' => 'commerce_pbd_order_in_submit_queue',
      'label' => t('Transaction belongs to Order in the queue "submit to PBD system"'),
      'parameter' => array(
        'commerce_payment_transaction' => array('label' => t('Transaction'), 'type' => 'commerce_payment_transaction'),
      ),
      'group' => t('Commerce Fulfillment'),
    ),
    'commerce_pbd_order_all_products_shipped' => array(
      'base' => 'commerce_pbd_order_all_products_shipped',
      'label' => t("All order's product line items have been shipped"),
      'parameter' => array(
        'commerce_order' => array('label' => t('Order'), 'type' => 'commerce_order'),
      ),
      'group' => t('Commerce Fulfillment'),
    ),
  );
}

/**
 * Implements hook_rules_action_info().
 */
function commerce_pbd_rules_action_info() {
  $actions = array();

  $actions['commerce_pbd_order_submit_queue_add'] = array(
    'label' => t('Add the Order to the "Order Submit to PBD" Queue'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order', array(), array('context' => 'a drupal commerce order')),
        'skip save' => TRUE,
      ),
    ),
    'group' => t('Commerce Fulfillment'),
    'callbacks' => array(
      'execute' => 'commerce_pbd_order_submit_queue_add',
    ),
  );

  $actions['commerce_pbd_order_submit_on_payment_transaction_update'] = array(
    'label' => t('Add the Order to the "Order Submit to PBD" Queue when a payment transaction authorized'),
    'parameter' => array(
      'commerce_payment_transaction' => array(
        'type' => 'commerce_payment_transaction',
        'label' => t('Payment Transaction', array(), array('context' => 'a drupal commerce payment transaction')),
      ),
    ),
    'group' => t('Commerce Fulfillment'),
    'callbacks' => array(
      'execute' => 'commerce_pbd_order_submit_on_payment_transaction_update',
    ),
  );

  $actions['commerce_pbd_rules_order_cancel'] = array(
    'label' => t('Cancel an Order in PBD System'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order', array(), array('context' => 'a drupal commerce order')),
        'skip save' => TRUE,
      ),
    ),
    'group' => t('Commerce Fulfillment'),
    'callbacks' => array(
      'execute' => 'commerce_pbd_rules_order_cancel',
    ),
  );
  return $actions;
}
