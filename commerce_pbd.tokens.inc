<?php

/**
 * @file
 * Defines additional tokens for order email.
 */

/**
 * Implements hook_token_info_alter().
 */
function commerce_pbd_token_info_alter(&$data) {
  $data['tokens']['commerce-order']['commerce-pbd-backordered-items'] = array(
    'name' => t('Order Items'),
    'description' => t('A table containing order items.'),
  );
  $data['tokens']['commerce-order']['commerce-pbd-tracking-number'] = array(
    'name' => t('Order Tracking Number(s)'),
    'description' => t('Orddr shipping tracking number(s).'),
  );
}

/**
 * Implements hook_tokens().
 */
function commerce_pbd_tokens($type, $tokens, array $data = array(), array $options = array()) {

  $replacements = array();

  if ($type == 'commerce-order' && !empty($data['commerce-order'])) {
    $order = $data['commerce-order'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'commerce-pbd-tracking-number':
          $wrapper = entity_metadata_wrapper('commerce_order', $order);
          $replacements[$original] = $wrapper->field_pbd_tracking_number->value();
          break;

        case 'commerce-pbd-backordered-items':
          $backorder_items = commerce_pbd_backordered_items($order);
          $replacements[$original] = $backorder_items;
          break;
      }
    }
  }
  return $replacements;
}
