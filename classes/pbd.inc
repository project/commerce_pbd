<?php

/**
 * @file
 * Implements PBDWebServices class.
 */

/**
 * Class PBDWebServices.
 */
class PBDWebServices {

  private static $pbdSoapUrl;
  private static $clientId;
  private static $soapOptions;

  /**
   * Singleton instance of PBDWebServices.
   *
   * @var PBDWebServices
   */
  protected static $instance = NULL;

  /**
   * PBDWebServices constructor.
   *
   * @param string $soap_url
   *   SOAP request URL.
   * @param string $client_id
   *   The Client ID.
   *
   * @throws \Exception
   */
  public function __construct($soap_url = '', $client_id = '') {
    if (empty($soap_url) || empty($client_id)) {
      self::$pbdSoapUrl = variable_get('commerce_pbd_soap_url', '');
      self::$clientId = variable_get('commerce_pbd_client_id', FALSE);
    }
    else {
      self::$pbdSoapUrl = $soap_url;
      self::$clientId = $client_id;
    }

    if (empty(self::$pbdSoapUrl) || empty(self::$clientId)) {
      throw new Exception(t('PBD Web Services client account has not been configured!'));
    }
    self::$soapOptions = array(
      // Traces let us look at the actual SOAP messages later.
      'trace'       => 1,
      'exceptions'  => 1,
    );

    $success = self::ping();
    if (!$success) {
      throw new Exception('Unable to connect to PBD Web Services SOAP server');
    }
  }

  /**
   * Creates PBDWebServices object and stores it for singleton access.
   *
   * @return object
   *   The instance of PBDWebServices class.
   */
  public static function init() {
    try {
      return self::$instance = new self();
    }
    catch (Exception $e) {
      drupal_set_message($e->getMessage(), 'error');
      self::$instance = NULL;
      return NULL;
    }
  }

  /**
   * Gets singleton instance of PBDWebServices.
   *
   * @param bool $autoCreate
   *   The autoCreate flag.
   *
   * @return object
   *   PBDWebServices
   */
  public static function getInstance($autoCreate = FALSE) {
    if ($autoCreate === TRUE && !self::$instance) {
      self::init();
    }
    return self::$instance;
  }

  /**
   * Pings the web services server with ClientID.
   *
   * @return bool
   *   TRUE if successful, FALSE otherwise.
   */
  public static function ping() {
    $wsdl = self::$pbdSoapUrl . 'Ping?wsdl';
    $param = new StdClass();
    $param->clientId = self::$clientId;
    $webservice = new SoapClient($wsdl, self::$soapOptions);
    if (!$webservice) {
      return FALSE;
    }
    try {
      $result = $webservice->PingService($param);
      $request = $webservice->__getLastRequest();
      if ($result->pingServiceReturn) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    catch (SOAPFault $f) {
      // Handle the fault here.
      return FALSE;
    }
  }

  /**
   * Checks the item availability in PBD system.
   *
   * @param string $item_sku
   *   SKU of the product.
   *
   * @return bool|int
   *   FALSE if not in the system; otherwise, ## of units available.
   *
   * @throws Exception
   */
  public static function itemAvailability($item_sku) {

    $wsdl = self::$pbdSoapUrl . 'itemavailability?wsdl';
    $request = new StdClass();
    $request->in0 = new StdClass();
    $request->in0->genericServiceHeader = self::fillOutRequestHeader('ITEMAVAIL');

    $serviceBody = new StdClass();
    $serviceBody->ItemAvailability = array();
    $serviceBody->ItemAvailability['ItemAvailabilityAttributes'] = array();
    $serviceBody->ItemAvailability['ItemAvailabilityAttributes']['RequesterID'] = self::$clientId;
    $serviceBody->ItemAvailability['ItemAvailabilityAttributes']['Action'] = 'CHECKQTY';
    $serviceBody->ItemAvailability['ItemCode'] = $item_sku;

    $request->in0->serviceBody = format_xml_elements((array) $serviceBody);
    $request->in0->serviceBody = str_replace('<ItemAvailability>', '<ItemAvailability xmlns="http://www.pbd.com">', $request->in0->serviceBody);

    $webservice = new SoapClient($wsdl, self::$soapOptions);
    if (!$webservice) {
      return FALSE;
    }
    try {

      $response = $webservice->performService($request);
      $request = $webservice->__getLastRequest();

      $xml_response_header = $response->performServiceReturn->outputHeader;
      $response_header = simplexml_load_string($xml_response_header);

      $xml_response_body = $response->performServiceReturn->outputBody;
      $response_body = simplexml_load_string($xml_response_body);

      $available = (int) $response_body->AvailableQuantity;
      if ($response_header->ErrorCode == -1 || (isset($response_body->ItemAvailabilityErrors) && !empty($response_body->ItemAvailabilityErrors->ErrorMessage))) {
        $menu_callback = strpos($_GET['q'], 'availability') !== FALSE;
        if ($menu_callback) {
          throw new Exception($response_body->ItemAvailabilityErrors->ErrorMessage);
        }
        else {
          $error_message = $response_header->ErrorCode == 0 ? (string) $response_body->ItemAvailabilityErrors->ErrorMessage : (string) $response_header->ErrorMessage;
          watchdog('commerce_pbd', 'itemAvailability error: !error', array('!error' => $error_message));
          return FALSE;
        }
      }

      return $available;
    }
    catch (SOAPFault $f) {
      // Handle the fault here.
      return 'soap_fault: ' . $f->getMessage();
    }

  }

  /**
   * Submits an order to PBD system.
   *
   * @param object $order
   *   Commerce order.
   *
   * @return bool|int
   *   FALSE if service is not available; order number if successful.
   *
   * @throws Exception
   */
  public static function orderSubmission($order) {

    $wsdl = self::$pbdSoapUrl . 'ordersubmission?wsdl';
    $request = new StdClass();
    $request->input = new StdClass();
    $request->input->genericServiceHeader = self::fillOutRequestHeader('ORDERSBMT');

    $serviceBody = array();
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    // Order Attributes.
    $serviceBody['OrderAttributes'] = array();
    $serviceBody['OrderAttributes']['RequesterID'] = self::$clientId;
    $serviceBody['OrderAttributes']['Action'] = 'ORDERSBMT';
    // Order Header.
    $serviceBody['OrderHeader'] = array();
    // Valid values range from 1 thru 999999999999999.
    $serviceBody['OrderHeader']['OrderID'] = $order->order_id;
    // Mandatory. Format should be YYYY-MM-DD.
    $serviceBody['OrderHeader']['DateOrdered'] = format_date($order->created, 'custom', 'Y-m-d');
    $serviceBody['OrderHeader']['Shipping'] = '';
    // Tax charge for the order, integer.
    // For example, $12.99 should come across as 1299.
    $order_price_components = $order->commerce_order_total['und'][0]['data']['components'];
    if (count($order_price_components) > 1) {
      // In addition to base_price we may have there tax and shipping.
      foreach ($order_price_components as $component) {
        if (strpos($component['name'], 'tax') !== FALSE) {
          $serviceBody['OrderHeader']['Tax'] = round($component['price']['amount']);
        }
      }
    }

    $billing_profile = commerce_customer_profile_load($order->commerce_customer_billing['und'][0]['profile_id']);
    $shipping_profile = commerce_customer_profile_load($order->commerce_customer_shipping['und'][0]['profile_id']);
    $shipping_profile_wrapper = entity_metadata_wrapper('commerce_customer_profile', $shipping_profile);
    // Since PBD is not going to use billing info (payment gateway is in charge
    // of that) and the billing info may not be available
    // (PO or check purchase), we are going to use shipping info instead
    // of the billing info, which is mandatory for PBD order submit service.
    $serviceBody['OrderHeader']['BillToContactInformation'] = array();
    $serviceBody['OrderHeader']['BillToContactInformation']['CustomerNumber'] = $order->uid;
    $serviceBody['OrderHeader']['BillToContactInformation']['Name'] = substr($shipping_profile_wrapper->commerce_customer_address->name_line->value(), 0, 40);
    $serviceBody['OrderHeader']['BillToContactInformation']['Email'] = substr($order->mail, 0, 40);

    $serviceBody['OrderHeader']['BillToAddress'] = array();
    $serviceBody['OrderHeader']['BillToAddress']['AddressLine1'] = substr($shipping_profile_wrapper->commerce_customer_address->thoroughfare->value(), 0, 40);
    $serviceBody['OrderHeader']['BillToAddress']['AddressLine2'] = substr($shipping_profile_wrapper->commerce_customer_address->premise->value(), 0, 40);
    // Mandatory.
    $serviceBody['OrderHeader']['BillToAddress']['City'] = substr($shipping_profile_wrapper->commerce_customer_address->locality->value(), 0, 25);
    // Mandatory for US and Canadian orders.
    // Should be blank for all other countries.
    $serviceBody['OrderHeader']['BillToAddress']['State'] = substr($shipping_profile_wrapper->commerce_customer_address->administrative_area->value(), 0, 3);
    $serviceBody['OrderHeader']['BillToAddress']['Zip'] = substr($shipping_profile_wrapper->commerce_customer_address->postal_code->value(), 0, 12);
    $serviceBody['OrderHeader']['BillToAddress']['Country'] = substr($shipping_profile_wrapper->commerce_customer_address->country->value(), 0, 40);

    $serviceBody['OrderHeader']['ShipToContactInformation'] = array();
    $serviceBody['OrderHeader']['ShipToContactInformation']['CustomerNumber'] = $order->uid;
    $serviceBody['OrderHeader']['ShipToContactInformation']['Name'] = substr($shipping_profile_wrapper->commerce_customer_address->name_line->value(), 0, 40);
    $serviceBody['OrderHeader']['ShipToContactInformation']['ShipToFirstName'] = substr($shipping_profile_wrapper->commerce_customer_address->first_name->value(), 0, 40);
    $serviceBody['OrderHeader']['ShipToContactInformation']['ShipToLastName'] = substr($shipping_profile_wrapper->commerce_customer_address->last_name->value(), 0, 40);
    $serviceBody['OrderHeader']['ShipToContactInformation']['Email'] = substr($order->mail, 0, 40);

    $serviceBody['OrderHeader']['ShipToAddress'] = array();
    $serviceBody['OrderHeader']['ShipToAddress']['AddressLine1'] = substr($shipping_profile_wrapper->commerce_customer_address->thoroughfare->value(), 0, 40);
    $serviceBody['OrderHeader']['ShipToAddress']['AddressLine2'] = substr($shipping_profile_wrapper->commerce_customer_address->premise->value(), 0, 40);
    // Mandatory.
    $serviceBody['OrderHeader']['ShipToAddress']['City'] = substr($shipping_profile_wrapper->commerce_customer_address->locality->value(), 0, 25);
    // Mandatory for US and Canadian orders.
    // Should be blank for all other countries.
    $serviceBody['OrderHeader']['ShipToAddress']['State'] = substr($shipping_profile_wrapper->commerce_customer_address->administrative_area->value(), 0, 3);
    // Mandatory.
    $serviceBody['OrderHeader']['ShipToAddress']['Zip'] = substr($shipping_profile_wrapper->commerce_customer_address->postal_code->value(), 0, 12);
    // Mandatory.
    $serviceBody['OrderHeader']['ShipToAddress']['Country'] = substr($shipping_profile_wrapper->commerce_customer_address->country->value(), 0, 40);

    // For future functionality. Send blank for now.
    $serviceBody['OrderHeader']['OrderedBy'] = '';
    // Please contact PBD to get your company FOB your specific FOB codes.
    $serviceBody['OrderHeader']['ShippingMethod'] = 'U09';
    // Mandatory. Not currently used. Send blank for now.
    $serviceBody['OrderHeader']['MiscellaneousCode1'] = '';
    // Mandatory. Not currently used. Send blank for now.
    $serviceBody['OrderHeader']['MiscellaneousCode2'] = '';
    // Mandatory. Not currently used. Send blank for now.
    $serviceBody['OrderHeader']['MiscellaneousCode3'] = '';
    // Mandatory. Not currently used. Send blank for now.
    $serviceBody['OrderHeader']['MiscellaneousCode4'] = '';

    // Mandatory.
    $serviceBody['OrderHeader']['CreditCardInfo'] = array();
    // Optional. Token received from PBD from CreditCardTokenization Webservice.
    $serviceBody['OrderHeader']['CreditCardInfo']['CreditCardToken'] = '';
    // Mandatory. Required for Credit Card Orders.
    $serviceBody['OrderHeader']['CreditCardInfo']['CreditCardNumber'] = '';
    // Optional. This is the Name as it appears on the Credit Card.
    $serviceBody['OrderHeader']['CreditCardInfo']['CreditCardName'] = '';
    // Mandatory. Required for Credit Card Orders. Format: MMCCYY
    // For example, December 31, 2009 will come across as 122009.
    $serviceBody['OrderHeader']['CreditCardInfo']['CreditCardExpirationDate'] = '';
    // Mandatory. Required for Credit Card Orders.
    // Current accepted values for PBD: AMEX ; CBLN (Carte Blanche) ;
    // DCCB (Diner's Club) ;DISC ; ENRT (Enroute) ; JAL ; JCB ; MC ; VISA.
    $serviceBody['OrderHeader']['CreditCardInfo']['CreditCardType'] = '';
    // Optional, but preferred. - Credit card CVV2/CVC2/CID data.
    $serviceBody['OrderHeader']['CreditCardInfo']['CreditCardVerififcationData'] = '';

    // Specified as Mandatory in documentation. Works without it.
    $serviceBody['OrderHeader']['MarketingEffortCode'] = '';
    // Optional. This field may or may not be required
    // according to your contract.
    $serviceBody['OrderHeader']['CustomerServiceRepID'] = '';
    // Mandatory. Y/N indicator. Set to ‘Y’ does not change the Shipping Method.
    $serviceBody['OrderHeader']['RushFlag'] = 'N';
    // Optional.
    $serviceBody['OrderHeader']['PurchaseOrderNumber'] = '';
    // Optional. Should contain Fedex # for collect shipments.
    $serviceBody['OrderHeader']['DeliveryInstructions'] = '';
    if (isset($order->field_order_shipping_instruct)) {
      $shipping_instructions = $order_wrapper->field_order_shipping_instruct->value();
      if (!empty($shipping_instructions)) {
        // Optional. Should contain Fedex # for collect shipments.
        $serviceBody['OrderHeader']['DeliveryInstructions'] = $order_wrapper->field_order_shipping_instruct->value();
      }
    }
    // Optional. Customer Federal Tax Identification number
    // if there are a business or non-profit.
    $serviceBody['OrderHeader']['TaxFEIN'] = '';
    // Optional. For entities that perform “registered shopping”.
    $serviceBody['OrderHeader']['CustomerGroup'] = '';
    // Indicates the amount that shipping was discounted;
    // Two decimal places will be implied.
    // For example, $12.99 should come across as 1299.
    $serviceBody['OrderHeader']['ShippingDiscAmt'] = 0;
    // Promotion code which the shipping discount is attributed.
    $serviceBody['OrderHeader']['ShippingDiscPromo'] = '';

    // Order Details: line items
    // There can be 999 occurrences of this element.
    // There must be at least one line and at most
    // a maximum of 999 lines for an order.
    $item_num = 0;
    foreach ($order->commerce_line_items['und'] as $line_item_id) {
      $item = commerce_line_item_load($line_item_id['line_item_id']);
      if ($item->type == 'product') {
        $product_id = $item->commerce_product['und'][0]['product_id'];
        $product = commerce_product_load($product_id);
        $order_detail = array('key' => 'OrderDetail', 'value' => array());
        $order_detail['value']['LineID'] = ++$item_num;
        $order_detail['value']['ItemCode'] = $product->sku;
        $order_detail['value']['ItemName'] = strlen($product->title) > 30 ? substr($product->title, 0, 30) : $product->title;
        $order_detail['value']['Quantity'] = (int) $item->quantity;
        $order_detail['value']['UnitPrice'] = round($product->commerce_price['und'][0]['amount']);
        $order_detail['value']['ListPrice'] = $order_detail['value']['UnitPrice'];
        $order_detail['value']['ShipToContactInformation'] = $serviceBody['OrderHeader']['ShipToContactInformation'];
        $order_detail['value']['ShipToAddress'] = $serviceBody['OrderHeader']['ShipToAddress'];
        $serviceBody[] = $order_detail;
      }
      elseif ($item->type == 'shipping') {
        // Shipping charge for the order, integer.
        // For example, $12.99 should come across as 1299.
        $serviceBody['OrderHeader']['Shipping'] = round($item->commerce_unit_price['und'][0]['amount']);
        // Shipping Method, e.g. 'UPS Next Day Air':
        $shipping_method = $item->line_item_label;
        $codes = variable_get('commerce_pbd_shipping_codes', array());
        // Map the service to  PBD shipping method code.
        if (isset($codes[$shipping_method])) {
          $serviceBody['OrderHeader']['ShippingMethod'] = $codes[$shipping_method];
        }
        else {
          // UPS Ground by default:
          $serviceBody['OrderHeader']['ShippingMethod'] = 'U01';
        }
      }
    }

    $serviceBody['OrderNote'] = array();
    $serviceBody['OrderNote']['NoteText'] = '';

    $serviceBody = array('OrderSubmitRequest' => $serviceBody);

    $serviceBodyXML = format_xml_elements($serviceBody);
    $serviceBodyXML = str_replace('<OrderSubmitRequest>', '<OrderSubmitRequest xmlns="http://www.pbd.com">', $serviceBodyXML);
    $serviceBodyXML = str_replace('<OrderAttributes>', '<OrderAttributes xmlns="http://www.pbd.com">', $serviceBodyXML);
    $serviceBodyXML = str_replace('<OrderHeader>', '<OrderHeader xmlns="http://www.pbd.com">', $serviceBodyXML);
    $serviceBodyXML = str_replace('<OrderNote>', '<OrderNote xmlns="http://www.pbd.com">', $serviceBodyXML);
    $request->input->serviceBody = $serviceBodyXML;

    $webservice = new SoapClient($wsdl, self::$soapOptions);
    if (!$webservice) {
      return FALSE;
    }

    try {
      $response = $webservice->performService($request);
      $request = htmlspecialchars_decode($webservice->__getLastRequest());

      $xml_response_header = $response->performServiceReturn->outputHeader;
      $response_header = simplexml_load_string($xml_response_header);
      $error_message = (string) $response_header->ErrorMessage;
      if ((string) $response_header->Status == 'false' && !empty($error_message)) {
        if ((string) $response_header->ErrorCode != '5005') {
          // Code 5005 is 'Content of element "Shipping" is incomplete',
          // we'll process it below.
          $error_message = $response_header->ErrorMessage . '; ErrorCode: ' . $response_header->ErrorCode;
          self::processError($order, $error_message);
        }
      }

      $xml_response_body = $response->performServiceReturn->outputBody;
      $response_body = simplexml_load_string($xml_response_body);
      $return_status = (int) $response_body->ReturnAttributes->ReturnStatus;
      $order_number = (int) $response_body->ReturnAttributes->OrderNumber;
      $error_message = (string) $response_body->OrderSubmitError->ErrorMsg;
      if ($order_number) {
        $order->field_pbd_order_number['und'][0]['value'] = $order_number;
      }
      $order->field_pbd_doc_type['und'][0]['value'] = (string) $response_body->ReturnAttributes->DocumentType;
      $order->field_pbd_company_num['und'][0]['value'] = (string) $response_body->ReturnAttributes->CompanyNumber;

      if ($return_status && !empty($error_message)) {
        if (is_numeric($order_number) && $order_number != 0 && strpos($error_message, 'Duplicate Order') !== FALSE) {
          // We had a situation that previous Order Submit resulted in
          // 'Internal Server Error', so the order number was not received.
          // Then subsequent submit caused error message
          // 'ORD0001: Duplicate Order-NNNNN, Order not Processed'.
          watchdog('commerce_pbd', 'OrderSubmitRequest error: !error', array('!error' => $error_message));
        }
        elseif (strpos($error_message, 'Item is Obsolete') !== FALSE) {
          $order->status = 'rejected_obsolete';
          $order->log = t('Order rejected by PBD as having Obsolete Product.');
          $order->revision = TRUE;
        }
        elseif (strpos($error_message, '"Shipping" is incomplete') !== FALSE) {
          $order->status = 'rejected_no_shipping_info';
          $order->log = t('Order rejected by PBD as having no Shipping Service info.');
          $order->revision = TRUE;
        }
        elseif (strpos($error_message, 'ITM0001') !== FALSE) {
          $order->status = 'rejected_invalid_item';
          $order->log = t('Order rejected by PBD as having invalid item, error: @error.', array('@error' => $error_message));
          $order->revision = TRUE;
        }
        elseif (strpos($error_message, 'FOB0017') !== FALSE
          || strpos($error_message, 'FOB0010') !== FALSE
          || strpos($error_message, 'FOB0011') !== FALSE) {
          $order->status = 'rejected_invalid_address';
          $order->log = t('Order rejected by PBD as having invalid address, error: @error.', array('@error' => $error_message));
          $order->revision = TRUE;
        }
        else {
          self::processError($order, $error_message);
        }
      }
      // Saving order parameters supplied by PBD:
      commerce_order_save($order);
      return $order_number;
    }
    catch (SOAPFault $f) {
      // Handle the fault here.
      $error_message = $f->getMessage();
      self::processError($order, $error_message);
    }
  }

  /**
   * Processes the arbitrary error message.
   *
   * @param object $order
   *   Commerce Order.
   * @param string $error_message
   *   The error message.
   *
   * @throws \Exception
   */
  private static function processError($order, $error_message) {
    if ($order->status != 'rejected_submission_failed') {
      $order->status = 'rejected_submission_failed';
      $order->log = t('Order submission to PBD failed, error: @error.', array('@error' => $error_message));
      $order->revision = TRUE;
      commerce_order_save($order);
    }
    throw new Exception($error_message);
  }

  /**
   * Checks order status in PBD system.
   *
   * @param object $order
   *   Commerce order to check the status of.
   *
   * @return bool
   *   TRUE if successful, FALSE otherwise.
   *
   * @throws Exception
   */
  public static function orderStatus($order) {

    $wsdl = self::$pbdSoapUrl . 'orderstatus?wsdl';
    $request = new StdClass();
    $request->in0 = new StdClass();
    $request->in0->genericServiceHeader = self::fillOutRequestHeader('ORDERSTS');

    $orderStatus = array();
    // Order Status Attributes.
    $orderStatus['OrderStatusAttributes'] = array();
    $orderStatus['OrderStatusAttributes']['RequesterID'] = self::$clientId;
    $orderStatus['OrderStatusAttributes']['Action'] = 'ORDERSTS';

    // Order Status Information.
    $orderStatus['OrderStatusInformation'] = array();
    // Too big value generates an error "the value is out of the range
    // (maxInclusive specifies 99,999,999).",
    // contrary to documentation for order submit.
    $orderStatus['OrderStatusInformation']['ClientOrderNumber'] = $order->order_id;
    $orderStatus['OrderStatusInformation']['PBDOrderNumber'] = $order->field_pbd_order_number['und'][0]['value'];
    $orderStatus['OrderStatusInformation']['PBDDocumentType'] = $order->field_pbd_doc_type['und'][0]['value'];
    $orderStatus['OrderStatusInformation']['PBDCompanyNumber'] = $order->field_pbd_company_num['und'][0]['value'];

    $orderStatus = array('OrderStatus' => $orderStatus);

    $orderStatusXML = format_xml_elements($orderStatus);
    $orderStatusXML = str_replace('<OrderStatus>', '<OrderStatus xmlns="http://www.pbd.com">', $orderStatusXML);
    $orderStatusXML = str_replace('<OrderStatusAttributes>', '<OrderStatusAttributes xmlns="http://www.pbd.com">', $orderStatusXML);
    $orderStatusXML = str_replace('<OrderStatusInformation>', '<OrderStatusInformation xmlns="http://www.pbd.com">', $orderStatusXML);

    $request->in0->serviceBody = $orderStatusXML;

    $webservice = new SoapClient($wsdl, self::$soapOptions);
    if (!$webservice) {
      return FALSE;
    }

    try {
      $response = $webservice->performService($request);
      $request = htmlspecialchars_decode($webservice->__getLastRequest());

      $xml_response_header = $response->performServiceReturn->outputHeader;
      $response_header = simplexml_load_string($xml_response_header);
      $error_message = (string) $response_header->ErrorMessage;
      if ((string) $response_header->Status == 'false' && !empty($error_message)) {
        throw new Exception($response_header->ErrorMessage . '; ErrorCode: ' . $response_header->ErrorCode);
      }

      $xml_response_body = $response->performServiceReturn->outputBody;
      $response_body = simplexml_load_string($xml_response_body);

      $order_hold_code = (string) $response_body->OrderStatusReturn->OrderHoldCode;
      $order_hold_code_desc = (string) $response_body->OrderStatusReturn->OrderHoldCodeD;
      // OrderTracking - if more than one shipment it will come as array:
      $order_tracking = $tracking_num = array();
      foreach ($response_body->OrderStatusReturn->OrderTracking as $orderTrackingItem) {
        // We may have more than one tracking number
        // for the same invoice number:
        $order_tracking[(string) $orderTrackingItem->InvoiceNBR][] = (string) $orderTrackingItem->TrackingNBR;
        $tracking_num[] = (string) $orderTrackingItem->TrackingNBR;
      }
      $invoice_num = array_keys($order_tracking);

      if (!empty($response_body->OrderStatusError)) {
        $error_message = '';
        foreach ($response_body->OrderStatusError as $error) {
          $error_message .= (string) $error->ErrorMessage . ' ';
        }
        throw new Exception($error_message);
      }

      $order_changed = FALSE;
      $order_status_code = NULL;
      $order_status = $order->status;

      if (!empty($order_hold_code)) {
        $order_status_code = $order_hold_code;
        $order_status = 'on_hold';
      }

      if ((!empty($order_hold_code) && (!isset($order->field_pbd_order_hold_code['und']) || $order_hold_code != $order->field_pbd_order_hold_code['und'][0]['value']))) {
        $order->field_pbd_order_hold_code['und'][0]['value'] = $order_hold_code;
        $order_changed = TRUE;
      }
      // Invoice numbers - could be several:
      if (!empty($invoice_num) && isset($order->field_pbd_invoice_number)) {
        $order_invoice_num = array();
        foreach ($invoice_num as $inv_id => $inv_num) {
          $order_invoice_num[$inv_id]['value'] = $inv_num;
        }
        if (!isset($order->field_pbd_invoice_number['und']) || $order_invoice_num != $order->field_pbd_invoice_number['und']) {
          $order->field_pbd_invoice_number['und'] = $order_invoice_num;
          $order_changed = TRUE;
        }
      }
      // Tracking numbers: could be several even for a single invoice:
      if (!empty($tracking_num) && isset($order->field_pbd_tracking_number)) {
        $order_tracking_num = array();
        foreach ($tracking_num as $track_id => $track_num) {
          $order_tracking_num[$track_id]['value'] = $track_num;
        }
        // TODO: make comparison function,
        // because after saving we get there 'format' and 'safe_value'.
        if (!isset($order->field_pbd_tracking_number['und']) ||  $order_tracking_num != $order->field_pbd_tracking_number['und']) {
          $order->field_pbd_tracking_number['und'] = $order_tracking_num;
          $order_changed = TRUE;
        }
      }

      $order_status_map = array(
        'processing' => 'processing',
        'P' => 'pending_shipment',
        'B' => 'backordered',
        'S' => 'shipped',
        'N' => 'canceled',
      );
      // Updating the Line Items:
      if (!empty($response_body->OrderStatusReturn->OrderDetails)) {
        // Get an array of line items.
        $line_items = array();
        foreach ($order->commerce_line_items['und'] as $line_item_id) {
          $line_item = commerce_line_item_load($line_item_id['line_item_id']);
          if ($line_item->type == 'product') {
            $product = commerce_product_load($line_item->commerce_product['und'][0]['product_id']);
            $line_items[$product->sku] = $line_item;
          }
          elseif ($line_item->type == 'shipping') {
            $line_items['shipping'] = $line_item;
          }
        }

        foreach ($response_body->OrderStatusReturn->OrderDetails as $lineItem) {
          $item_code_parts = explode('.', $lineItem->ItemCode);
          $commerce_line_item = $line_items[$item_code_parts[1]];
          if (!$commerce_line_item) {
            continue;
          }

          if ((string) $lineItem->StatusCode == 'B' && (string) $lineItem->StatusCode != $commerce_line_item->field_pbd_line_item_status['und'][0]['value']) {
            $order->line_items_backordered = TRUE;
          }

          if ((string) $lineItem->StatusCode == 'S' && (string) $lineItem->StatusCode != $commerce_line_item->field_pbd_line_item_status['und'][0]['value']) {
            $order->line_items_shipped[] = $commerce_line_item->line_item_id;
          }

          $commerce_line_item->field_pbd_line_item_status['und'][0]['value'] = (string) $lineItem->StatusCode;
          if (!$order_status_code) {
            $order_status_code = (string) $lineItem->StatusCode;
          }
          elseif ($order_status_code != (string) $lineItem->StatusCode && $order_status_code != 'processing' && $order_status != 'on_hold') {
            $order_status_code = 'processing';
          }
          // Invoice number and tracking numbers for the line item:
          $commerce_line_item->field_pbd_product_invoice_number = array();

          if ($item_invoice_num = (int) $lineItem->InvoiceNBR) {
            $commerce_line_item->field_pbd_product_invoice_number['und'][0]['value'] = $item_invoice_num;
            $commerce_line_item->field_pbd_tracking_number = array();
            foreach ($order_tracking[$item_invoice_num] as $track_id => $track_num) {
              // Could be several shipments for the same line item
              // - if all the items do not fit into the same box.
              $commerce_line_item->field_pbd_tracking_number['und'][$track_id]['value'] = $track_num;
            }
          }
          $invoice_date = self::formatDateToIso((string) $lineItem->InvoiceDate);
          if ($invoice_date != NULL) {
            $commerce_line_item->field_pbd_invoice_date['und'][0]['value'] = $invoice_date;
          }
          $ship_date = self::formatDateToIso((string) $lineItem->ShipDate);
          if ($ship_date != NULL) {
            $commerce_line_item->field_pbd_ship_date['und'][0]['value'] = $ship_date;
          }
          $shipping_method = (string) $lineItem->FOBCodeDescription;
          if ($shipping_method) {
            $commerce_line_item->field_pbd_shipping_method['und'][0]['value'] = $shipping_method;
          }
          // Saving the line item with all new data.
          commerce_line_item_save($commerce_line_item);
        }
        if ($order_status_code && empty($order_hold_code)) {
          $order_status = $order_status_map[$order_status_code];
        }
        if ($order_status != $order->status) {
          if ($order_status == 'shipped') {
            $ship_date_timestamp = strtotime($ship_date);
            $order->log = t('Order shipped by PBD on !date, tracking # !track', array(
              '!date' => date('M j, Y', $ship_date_timestamp),
              '!track' => $track_num,
            ));
          }
          elseif ($order_status == 'on_hold' && !empty($order_hold_code_desc)) {
            $log_message = t('Order put on hold by PBD');
            $order->log = $log_message . ': ' . $order_hold_code_desc;
          }
          else {
            $order->log = t('Order status changed by PBD to "!status"', array('!status' => $order_status));
          }
          $order->status = $order_status;
          $order->revision = TRUE;
          $order_changed = TRUE;
        }
      }

      if (!empty($order->line_items_shipped) && isset($line_items['shipping'])) {
        $order->line_items_shipped[] = $line_items['shipping']->line_item_id;
      }

      if ($order_changed) {
        // Saving order parameters supplied by PBD.
        commerce_order_save($order);
      }
      return TRUE;
    }
    catch (SOAPFault $f) {
      // Handle the fault here.
      throw new Exception($f->getMessage());
    }

  }

  /**
   * Cancels an order in PBD system.
   *
   * @param object $order
   *   Commerce order to cancel.
   *
   * @return bool
   *   TRUE if successful, FALSE otherwise.
   *
   * @throws Exception
   */
  public static function orderCancel($order) {
    $wsdl = self::$pbdSoapUrl . 'ordercancel?wsdl';
    $request = new StdClass();
    $request->in0 = new StdClass();
    $request->in0->genericServiceHeader = self::fillOutRequestHeader('ORDERCANCL');

    $orderCancel = array();
    // Order Cancel Attributes.
    $orderCancel['OrderCancelAttributes'] = array();
    $orderCancel['OrderCancelAttributes']['RequesterID'] = self::$clientId;
    $orderCancel['OrderCancelAttributes']['Action'] = 'ORDERCANCL';

    // Order Cancel Information.
    $orderCancel['OrderInformation'] = array();
    $orderCancel['OrderInformation']['PBDOrderNumber'] = $order->field_pbd_order_number['und'][0]['value'];
    $orderCancel['OrderInformation']['PBDDocumentType'] = $order->field_pbd_doc_type['und'][0]['value'];
    $orderCancel['OrderInformation']['PBDCompanyNumber'] = $order->field_pbd_company_num['und'][0]['value'];

    $orderCancel = array('OrderCancel' => $orderCancel);

    $orderCancelXML = format_xml_elements($orderCancel);
    $orderCancelXML = str_replace('<OrderCancel>', '<OrderCancel xmlns="http://www.pbd.com">', $orderCancelXML);
    $orderCancelXML = str_replace('<OrderCancelAttributes>', '<OrderCancelAttributes xmlns="http://www.pbd.com">', $orderCancelXML);
    $orderCancelXML = str_replace('<OrderInformation>', '<OrderInformation xmlns="http://www.pbd.com">', $orderCancelXML);

    $request->in0->serviceBody = $orderCancelXML;

    $webservice = new SoapClient($wsdl, self::$soapOptions);
    if (!$webservice) {
      return FALSE;
    }

    try {
      $response = $webservice->performService($request);
      $request = htmlspecialchars_decode($webservice->__getLastRequest());

      $xml_response_header = $response->performServiceReturn->outputHeader;
      $response_header = simplexml_load_string($xml_response_header);
      $error_message = (string) $response_header->ErrorMessage;
      if ((string) $response_header->Status == 'false' && !empty($error_message)) {
        throw new Exception($response_header->ErrorMessage . '; ErrorCode: ' . $response_header->ErrorCode);
      }

      $xml_response_body = $response->performServiceReturn->outputBody;
      $response_body = simplexml_load_string($xml_response_body);

      if (!empty($response_body->OrderCancelError)) {
        $error_message = 'OrderCancelError: ';
        foreach ($response_body->OrderCancelError as $error) {
          $error_message .= (string) $error->ErrorMessage . ' ';
        }
        throw new Exception($error_message);
      }

      if ((int) $response_body->OrderCancelReturn->OrderCancelStatus <= 1) {
        $order->field_pbd_order_hold_code['und'][0]['value'] = '';
        $order->field_pbd_invoice_number['und'][0]['value'] = 0;
        $order->field_pbd_tracking_number['und'][0]['value'] = '';
        $order->status = 'canceled';
        commerce_order_save($order);
        return TRUE;
      }

      return FALSE;
    }
    catch (SOAPFault $f) {
      // Handle the fault here.
      throw new Exception($f->getMessage());
    }

  }

  /**
   * Fills out request header.
   *
   * @param string $service_name
   *   The service name.
   *
   * @return string
   *   request header XML string
   */
  public static function fillOutRequestHeader($service_name) {
    $serviceRequestHeader = array();
    // Required field of type String with Maximum size as 50:
    $serviceRequestHeader['MessageID'] = uniqid(ip_address() . '_', TRUE);
    $serviceRequestHeader['ClientID'] = self::$clientId;
    $serviceRequestHeader['ServiceName'] = $service_name;
    // This is currently the same for all services.
    $serviceRequestHeader['VersionNumber'] = '0000010';
    $serviceRequestHeader['TimeStamp'] = date("c", time());
    $serviceRequestHeaderXML = format_xml_elements(array('ServiceRequestHeader' => $serviceRequestHeader));
    // Adding namespace.
    $serviceRequestHeaderXML = str_replace('<ServiceRequestHeader>', '<ServiceRequestHeader xmlns="http://www.pbd.com">', $serviceRequestHeaderXML);
    return $serviceRequestHeaderXML;
  }

  /**
   * Formats PBD date to ISO format.
   *
   * @param string $pbd_date_string
   *   PBD date in YYYYMMDD format.
   *
   * @return mixed
   *   ISO Date string if successful; NULL otherwise
   */
  public static function formatDateToIso($pbd_date_string) {
    if (strlen($pbd_date_string) != 8) {
      return NULL;
    }
    $year = substr($pbd_date_string, 0, 4);
    $month = substr($pbd_date_string, 4, 2);
    $day = substr($pbd_date_string, 6, 2);
    // Validation:
    if (!is_numeric($year) || !is_numeric($month) || !is_numeric($day) || (int) $year < 2000
      || (int) $month < 1 || (int) $month > 12 || (int) $day < 1 || (int) $day > 31) {
      return NULL;
    }
    // Make the time at noon, so we should not worry about
    // time zone and day light saving.
    return $year . '-' . $month . '-' . $day . 'T12:00:00';
  }

}
