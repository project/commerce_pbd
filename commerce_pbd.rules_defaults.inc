<?php

/**
 * @file
 * Default rules hook implementations for the Commerce PBD module.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_pbd_default_rules_configuration() {
  $rules = array();

  // Add a reaction rule to add the order with shippable products to PBD order
  // submit queue when paid in full.
  $rule = rules_reaction_rule();
  $rule->label = t('When paid in full, add the order with shippable products to PBD order submit queue');
  $rule->active = TRUE;

  $rule
    ->event('commerce_payment_order_paid_in_full')
    ->condition('commerce_physical_rules_order_is_shippable', array('commerce_order:select' => 'commerce-order'))
    ->condition(rules_condition('commerce_pbd_order_in_pbd_system', array('commerce_order:select' => 'commerce-order'))->negate())
    ->condition(rules_condition('commerce_pbd_order_in_submit_queue', array('commerce_order:select' => 'commerce-order'))->negate())
    ->action(
      'commerce_pbd_order_submit_queue_add',
      array(
        'commerce_order:select' => 'commerce-order',
      )
    );

  $rules['commerce_pbd_order_submit_queue_add'] = $rule;

  // Add a reaction rule to change order status to "completed"
  // when all products shipped.
  $rule = rules_reaction_rule();
  $rule->label = t('When all products shipped and order paid in full, change order status to "completed"');
  $rule->active = TRUE;
  $rule
    ->event('commerce_payment_order_paid_in_full')
    ->condition('commerce_physical_rules_order_is_shippable', array('commerce_order:select' => 'commerce-order'))
    ->condition('commerce_pbd_order_in_pbd_system', array('commerce_order:select' => 'commerce-order'))
    ->condition('commerce_pbd_order_all_products_shipped', array('commerce_order:select' => 'commerce-order'))
    ->action('commerce_order_update_state', array(
      'commerce_order:select' => 'commerce-order',
      'order_state' => 'completed',
    ));

  $rules['commerce_pbd_order_completed'] = $rule;

  // Add a reaction rule to add the order to PBD order submit queue after
  // payment transaction inserted with Auth.net authorization.
  $rule = rules_reaction_rule();
  $rule->label = t('Add the order with shippable products to PBD order submit queue when authorizing payment transaction inserted (e.g. Auth.net authorization)');
  $rule->active = FALSE;
  $rule
    ->event('commerce_payment_transaction_insert')
    ->event('commerce_payment_transaction_update')
    ->condition('commerce_real_invoice_payment_authorized', array('commerce_payment_transaction:select' => 'commerce-payment-transaction'))
    ->condition('commerce_pbd_transaction_order_shippable', array('commerce_payment_transaction:select' => 'commerce-payment-transaction'))
    ->condition(rules_condition('commerce_pbd_transaction_order_in_pbd_system', array('commerce_payment_transaction:select' => 'commerce-payment-transaction'))->negate())
    ->condition(rules_condition('commerce_pbd_transaction_order_in_submit_queue', array('commerce_payment_transaction:select' => 'commerce-payment-transaction'))->negate())
    ->condition(rules_condition('commerce_pbd_transaction_order_rejected_by_pbd', array('commerce_payment_transaction:select' => 'commerce-payment-transaction'))->negate())
    ->action(
      'commerce_pbd_order_submit_on_payment_transaction_update',
      array(
        'commerce_payment_transaction:select' => 'commerce-payment-transaction',
      )
    );

  $rules['commerce_pbd_order_submit_on_payment_transaction_update'] = $rule;

  // Add a reaction rule to add the order to PBD order submit queue
  // on 'Order Shipping Authorized' event.
  $rule = rules_reaction_rule();
  $rule->label = t('Add an order with shippable products to PBD order submit queue on "Order Shipping Authorized" event');
  $rule->active = TRUE;
  $rule
    ->event('commerce_order_shipping_authorized')
    ->condition('commerce_physical_rules_order_is_shippable', array('commerce_order:select' => 'commerce-order'))
    ->condition(rules_condition('commerce_pbd_order_in_pbd_system', array('commerce_order:select' => 'commerce-order'))->negate())
    ->condition(rules_condition('commerce_pbd_order_in_submit_queue', array('commerce_order:select' => 'commerce-order'))->negate())
    ->condition(rules_condition('commerce_pbd_order_rejected_by_pbd', array('commerce_order:select' => 'commerce-order'))->negate())
    ->action('commerce_pbd_order_submit_queue_add', array('commerce_order:select' => 'commerce-order'));

  $rules['commerce_pbd_order_submit_on_order_shipping_authorized'] = $rule;

  // Add a reaction rule to add an order with shippable products to PBD order
  // submit queue when the order status changes to "To Be Fulfilled".
  $rule = rules_reaction_rule();

  $rule->label = t('Add the order with shippable products to PBD order submit queue when the order status changes to "To Be Fulfilled"');
  $rule->active = TRUE;

  $rule
    ->event('commerce_order_update')
    ->condition('data_is', array(
      'data:select' => 'commerce-order:status',
      'op' => '==',
      'value' => 'to_be_fulfilled',
    ))
    ->condition(rules_condition('data_is', array(
      'data:select' => 'commerce-order-unchanged:status',
      'value' => 'to_be_fulfilled',
    ))->negate())
    ->condition('commerce_physical_rules_order_is_shippable', array('commerce_order:select' => 'commerce-order'))
    ->condition(rules_condition('commerce_pbd_order_in_pbd_system', array('commerce_order:select' => 'commerce-order'))->negate())
    ->condition(rules_condition('commerce_pbd_order_in_submit_queue', array('commerce_order:select' => 'commerce-order'))->negate())
    ->action(
      'commerce_pbd_order_submit_queue_add',
      array(
        'commerce_order:select' => 'commerce-order',
      )
    );

  $rules['commerce_pbd_order_submit_on_to_be_fulfilled_status'] = $rule;

  // Add a reaction rule to cancel the order with shippable products
  // in PBD system when canceled in Commerce.
  $rule = rules_reaction_rule();
  $rule->label = t('Cancel the order with shippable products in PBD system when canceled in Commerce');
  $rule->active = TRUE;

  $rule
    ->event('commerce_order_update')
    ->condition('data_is', array(
      'data:select' => 'commerce-order:status',
      'op' => '==',
      'value' => 'canceled',
    ))
    ->condition('commerce_physical_rules_order_is_shippable', array('commerce_order:select' => 'commerce-order'))
    ->condition('commerce_pbd_order_in_pbd_system', array('commerce_order:select' => 'commerce-order'))
    ->action(
      'commerce_pbd_rules_order_cancel',
      array(
        'commerce_order:select' => 'commerce-order',
      )
    );

  $rules['commerce_pbd_order_cancel'] = $rule;

  // Add a reaction rule to cancel the order with shippable products
  // in PBD system when deleted in Commerce.
  $rule = rules_reaction_rule();
  $rule->label = t('Cancel the order with shippable products in PBD system when deleted in Commerce');
  $rule->active = TRUE;

  $rule
    ->event('commerce_order_delete')
    ->condition('commerce_physical_rules_order_is_shippable', array('commerce_order:select' => 'commerce-order'))
    ->condition('commerce_pbd_order_in_pbd_system', array('commerce_order:select' => 'commerce-order'))
    ->condition(rules_condition('data_is', array(
      'data:select' => 'commerce-order:status',
      'op' => '==',
      'value' => 'canceled',
    ))->negate())
    ->action(
      'commerce_pbd_rules_order_cancel',
      array(
        'commerce_order:select' => 'commerce-order',
      )
    );

  $rules['commerce_pbd_order_delete'] = $rule;

  // Add a reaction rule to notify customer and admin
  // when the order has been shipped.
  $rule = rules_reaction_rule();
  $rule->label = t('Notify customer and admin when the order has been shipped');
  $rule->active = TRUE;

  $rule
    ->event('commerce_order_shipped')
    ->condition('commerce_physical_rules_order_is_shippable', array('commerce_order:select' => 'commerce-order'))
    ->action('variable_email_mail', array(
      'to:select' => 'commerce-order:mail',
      'variable' => 'commerce_pbd_order_shipped_notify_customer_[mail_part]',
      'language' => 'default',
    ))
    ->action('variable_email_mail', array(
      'to:select' => 'site:mail',
      'variable' => 'commerce_pbd_order_shipped_notify_admin_[mail_part]',
      'language' => 'default',
    ));

  $rules['commerce_pbd_order_shipped_notify'] = $rule;

  // Add a reaction rule to notify customer and admin
  // when the order has backordered products.
  $rule = rules_reaction_rule();
  $rule->label = t('Notify customer and admin when products in the order have been backordered');
  $rule->active = TRUE;

  $rule
    ->event('commerce_pbd_products_backordered')
    ->condition('commerce_physical_rules_order_is_shippable', array('commerce_order:select' => 'commerce-order'))
    ->action('variable_email_mail', array(
      'to:select' => 'commerce-order:mail',
      'variable' => 'commerce_pbd_products_backordered_notify_customer_[mail_part]',
      'language' => 'default',
    ))
    ->action('variable_email_mail', array(
      'to:select' => 'site:mail',
      'variable' => 'commerce_pbd_products_backordered_notify_admin_[mail_part]',
      'language' => 'default',
    ));

  $rules['commerce_pbd_products_backordered_notify'] = $rule;

  return $rules;
}
